package com.sheremet;

/**
 * Created by max on 20/11/17.
 */

import javax.ejb.Remote;

@Remote
public interface HelloWorld
{
    public String sayHelloWorld();
}