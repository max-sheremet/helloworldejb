package com.sheremet;
import javax.ejb.Stateless;

/**
 * Created by max on 20/11/17.
 */
@Stateless
public class HelloWorldEng implements HelloWorld {
    @Override
    public String sayHelloWorld() {
        return "HelloWorld";
    }
}